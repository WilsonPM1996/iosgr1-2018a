//
//  WinnerViewController.swift
//  BullsEye
//
//  Created by Wilson Gabriel Ramos Bravo on 2/5/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class WinnerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func ExitButtonPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
